This is a Scala implementation of a solution for this Code Gold exercise: http://codegolf.stackexchange.com/questions/68551/pronoun-operation?newreg=c7ce8e80b49545bbaa0085a162d878cd


The challenge

Write a function or a program that takes a string composed of one or more subjective personal pronouns, separated by + signs, as an argument. The output must be a single pronoun, that is the result of the relation defined in the next paragraph.

Of course, feel free to edit as you wish to correct those grammatical errors that are surely present ;)

This is a code-golf challenge, so the shortest code wins.
The relation

The goal of this task is to translate in "math-speak" something that we use daily. How do we think of "You and I" ? Well, "We", don't we? So, if the input is You+I the output should be the string We. With more than two pronouns, it should look like: You+He+They -> You

The relation is defined as this table:

      I  You    He   We   You   They
I     I  We     We   We   We    We
You      You    You  We   You   You
He              He   We   You   They
We                   We   We    We
You                       You   You
They                            They

The 'You' Problem

Well, as you can see I'm not a native English speaker. In my language (Italian) there's a difference between the plural you (voi, in italian) and the singular you (tu). When I thought this challenge I didn't think in English, so there's the problem that it is impossible to recognize if I'm using plural or singular form. Fortunately (or obviously?), the input/output doesn't change in both forms, so you can use one of them and you will cover both cases!
About the input

The input will be always in the form "Pronoun+Pronoun+Pronoun" ... The pronouns will have the first letter in uppercase and the rest in lowercase. Pluses will be not be surrounded by spaces, only by pronouns. Empty input is possible and the result must be empty output.
Bonus

A little bonus of 15% if the program will manage two new pronouns: She and It. They are the same as He, obviously. Remember that this relation is reflexive, so She -> She and It -> It. Therefore, any combination that includes only She, He or It should output They.
Examples

You+I          -> We
You+He+They    -> You
I+You+He+They  -> We
They           -> They
They+You       -> You
You+You+I+You  -> We

For Bonus
She            -> She
She+He         -> They
I+It+He        -> We
It+You         -> You
