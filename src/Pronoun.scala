object pronouns extends Function1[String, String] {
  val p = List("I", "You", "He", "We", "They", "She", "It")
  def apply(in: String) = {
    val t: Array[String] = in split "[+]"
    p(t map (p indexOf _) reduceLeft(a(_, _)))
  }

  def a(f: Int, s: Int): Int = {
    f match {
      case 0 => s match {
          //I + I = I
        case 0 => 0
          // I + anything = We
        case _ => 3
      }
      case 1 => s match {
          // You + I, We, She or It = We
        case 0|3|5|6 => 3
          //You + You or They = You
        case _ => 1
      }
      case 2|5|6 => s match {
          //He, She or It + I or You = You or I + He, She or It
        case 0 | 1 => a(s, f)
          //He, She or It + He, We, They, She or It = They
        case _ => 4
      }
        // We + anything = We
      case 3 => 3
        //They + anything = anything + They
      case 4 => a(s, f)
    }
  }
}