object pronounsOriginal extends Function1[String, String] {
  def apply(in: String) = {
    val tokens: Array[String] = in split "[+]"
    tokens.reduceLeft(addPronouns(_, _))
  }

  def addPronouns(f: String, s: String): String = {
    f match {
      case f if f == s => f
      case "I" => "We"
      case "You" => s match {
        case "We"|"I" => "We"
        case _ => "You"
      }
      case "He" => s match {
        case "I" | "You" => addPronouns(s, f)
        case _ => s
      }
      case "We" => s match {
        case _ => "We"
      }
      case "They" => s match {
        case "They" => "They"
        case _ => addPronouns(s, f)
      }
    }
  }
}